#!/bin/sh

start_server()
{
  echo "Setting permissions"
  chown -R minecraft: ${MINECRAFT_MOD_HOME}
  echo "Unzipping mod"
  unzip ${MINECRAFT_MOD_ZIP} -d /tmp
  cp -rf /tmp/Space\ Astronomy\ 2\ Server\ Files\ -\ v1.3.0/* ${MINECRAFT_MOD_HOME}
  rm -rf /tmp

  cd ${MINECRAFT_MOD_HOME}
  echo "Removing DynamicSurroundings mod"
  rm -f mods/DynamicSurr*
  echo "Removing vanillatools mod"
  rm -f mods/vanillatools*
  echo "[EULA] Accepting EULA"
  echo "#By changing the setting below to TRUE you are indicating your agreement to our EULA \(https://account.mojang.com/documents/minecraft_eula\)." > eula.txt
  echo "eula=true" >> eula.txt

  java -server -Xms9g -Xmx9g -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=100 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:+AggressiveOpts -XX:+AlwaysPreTouch  -jar forge-1.10.2-12.18.3.2422-universal.jar nogui
}

start_server
